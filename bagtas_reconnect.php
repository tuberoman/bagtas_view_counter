<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The True and Proper Way of Worshipping God | RECONNECT</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css">
    <link href="css/styles.css?v=1.6" rel="stylesheet">
    <link href="css/queries.css?v=1.6" rel="stylesheet">
    <link href="css/jquery.fancybox.css" rel="stylesheet">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body onload="load()">
      <section class="hero" id="hero">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
              <h1 style="color:white;font-size: 64px;" class="animated bounceInDown">Lokal ng Bagtas</h1>
              <p style="color:white;font-size: 32px;"class="animated fadeInUpDelay">Total Views : <span id="viewCount">0</span></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 col-md-offset-5 text-center">
              <h2><a href="bagtas_reconnect_purok_1.php">PUROK 1 : <span id="viewCount1">0</span></a></h2>
              <h2><a href="bagtas_reconnect_purok_2.php">PUROK 2 : <span id="viewCount2">0</span></a></h2>
              <h2><a href="bagtas_reconnect_purok_3.php">PUROK 3 : <span id="viewCount3">0</span></a></h2>
              <h2><a href="bagtas_reconnect_purok_4.php">PUROK 4 : <span id="viewCount4">0</span></a></h2>
              <h2><a href="bagtas_reconnect_purok_5.php">PUROK 5 : <span id="viewCount5">0</span></a></h2>
              <h2><a href="bagtas_reconnect_purok_6.php">PUROK 6 : <span id="viewCount6">0</span></a></h2>
            </div>
          </div>
        </div>
      </section>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script type="text/javascript" src="view_count.json"></script>
      <script src="js/jquery.fancybox.pack.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
      <script src="js/scripts.js?v=1.7"></script>
      <script src="js/jquery.flexslider.js"></script>
      <script src="js/jquery.smooth-scroll.js"></script>
      <script src="js/modernizr.js"></script>
      <script src="js/waypoints.min.js"></script>
      <script>

        function load() {
          var request = new XMLHttpRequest();
          request.open("GET", "view_count.json", false);
          request.send(null);
          var my_JSON_object = JSON.parse(request.responseText);
          console.log(my_JSON_object);
          document.getElementById("viewCount").textContent=""+my_JSON_object.total_view_count;
          document.getElementById("viewCount1").textContent=""+my_JSON_object.purok_1;
          document.getElementById("viewCount2").textContent=""+my_JSON_object.purok_2;
          document.getElementById("viewCount3").textContent=""+my_JSON_object.purok_3;
          document.getElementById("viewCount4").textContent=""+my_JSON_object.purok_4;
          document.getElementById("viewCount5").textContent=""+my_JSON_object.purok_5;
          document.getElementById("viewCount6").textContent=""+my_JSON_object.purok_6;
        }
      </script>
    </body>
  </html>
