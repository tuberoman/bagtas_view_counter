<?php
include 'get_ip.php';
if(addIPAddressIfNotExists()){
  $dataArray = json_decode(file_get_contents("view_count.json"), true);

  $dataArray['total_view_count'] = $dataArray['total_view_count'] + 1;
  $dataArray['purok_3'] = $dataArray['purok_3'] + 1;

  $dataArrayString = json_encode($dataArray);
  $myfile = fopen("view_count.json", "w") or die("Unable to open file!");
  fwrite($myfile, $dataArrayString);
  fclose($myfile);

  header("Location: https://www.youtube.com/watch?v=HxRIJqGbGZA");
  die();
}else{
  print_r('You already reached the maximum views for the day. Please watch again tomorrow');
}
?>
