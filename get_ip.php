<?php

function addIPAddressIfNotExists(){
  $ipArrayData = json_decode(file_get_contents("visitor_ip.json"), true);
  $visitorIpAddress =  $_SERVER['REMOTE_ADDR'];

  if(!in_array( $visitorIpAddress ,$ipArrayData['visitor_ip_addresses']))
  {
      array_push($ipArrayData['visitor_ip_addresses'],$visitorIpAddress);
      $ipArrayString = json_encode($ipArrayData);
      $myfile = fopen("visitor_ip.json", "w") or die("Unable to open file!");
      fwrite($myfile, $ipArrayString);
      fclose($myfile);

      return true;
  }

  return false;
}
?>
